﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Capstone.Models;

namespace Capstone.DAL
{
    public class SiteSqlDAO : ISiteDAO
    {
        private string connectionString;

        public SiteSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IList<Site> SelectAvaliableSites(int campgroundid, DateTime arrivaldate, DateTime departuredate)
        {
            List<Site> list = new List<Site>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    string sql = "SELECT TOP 5 * FROM site AS s JOIN reservation AS r ON s.site_id = r.site_id WHERE s.campground_id = @campgroundid AND @arrival" + 
                        "> r.from_date AND @arrival < r.to_date AND @departure > r.from_date AND @departure < r.to_date";
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@arrival", arrivaldate);
                    cmd.Parameters.AddWithValue("@departure", departuredate);
                    cmd.Parameters.AddWithValue("@campgroundid", campgroundid);

                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Site obj = new Site();
                        obj.CampgroundId = Convert.ToInt32(reader["campground_id"]);
                        obj.Id = Convert.ToInt32(reader["site_id"]);
                        obj.SiteNumber = Convert.ToInt32(reader["site_number"]);
                        obj.MaxOccupancy = Convert.ToInt32(reader["max_occupancy"]);
                        obj.IsAccessible = Convert.ToBoolean(reader["accessible"]);
                        obj.MaxRVLength = Convert.ToInt32(reader["max_rv_length"]);
                        obj.HasUtilities = Convert.ToBoolean(reader["utilities"]);

                        list.Add(obj);
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"There was a ERROR {ex.Message}");
            }
            return list;
        }

        public IList<Site> SelectSitesByCampgroundId(int campgroundid)
        {
            throw new NotImplementedException();
        }
    }
}
