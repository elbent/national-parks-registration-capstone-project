﻿using Capstone.DAL;
using Capstone.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Capstone.Tests
{
    [TestClass]
    public class SiteSqlDAOTests : DAOTests
    {
        [TestMethod]
        public void GetSitesForBlackwoodsForTodayAndTomorrowShouldReturnTwo()
        {
            SiteSqlDAO dao = new SiteSqlDAO(connectionString);

            IList<Site> sites = dao.SelectAvaliableSites(blackwoodsid, DateTime.Today, DateTime.Today.AddDays(1));

            Assert.AreEqual(2, sites.Count);
        }
    }
}
