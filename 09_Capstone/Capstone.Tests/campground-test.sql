﻿-- This will be run inside a transaction scope during the tests.

DELETE FROM reservation;
DELETE FROM site;
DELETE FROM campground;
DELETE FROM park;

-- Fill in park info 
INSERT INTO park (name, location, establish_date, area, visitors, description)
VALUES ('Acadia', 'Maine', '1919-02-26', 47389, 2563129, 'Covering most of Mount Desert Island and other coastal islands, Acadia features the tallest mountain on the Atlantic coast of the United States, granite peaks, ocean shoreline, woodlands, and lakes. There are freshwater, estuary, forest, and intertidal habitats.');
declare @acadiaid int = (SELECT @@IDENTITY)

INSERT INTO park (name, location, establish_date, area, visitors, description)
VALUES ('Arches',	'Utah', '1929-04-12', 76518,	1284767, 'This site features more than 2,000 natural sandstone arches, including the famous Delicate Arch. In a desert climate, millions of years of erosion have led to these structures, and the arid ground has life-sustaining soil crust and potholes, which serve as natural water-collecting basins. Other geologic formations are stone columns, spires, fins, and towers.');
declare @archesid int = (SELECT @@IDENTITY)

-- Fill in campground info
-- Acadia Campgrounds
INSERT INTO campground (park_id, name, open_from_mm, open_to_mm, daily_fee) VALUES (@acadiaid, 'Blackwoods', 1, 12, 35.00);
declare @blackwoodsid int = (SELECT @@IDENTITY)
INSERT INTO campground (park_id, name, open_from_mm, open_to_mm, daily_fee) VALUES (@acadiaid, 'Seawall', 5, 9, 30.00);
declare @seawallid int = (SELECT @@IDENTITY)

-- Arches Campgrounds
INSERT INTO campground (park_id, name, open_from_mm, open_to_mm, daily_fee) VALUES (@archesid, 'Devil''s Garden', 1, 12, 25.00);
declare @devilsid int = (SELECT @@IDENTITY)
INSERT INTO campground (park_id, name, open_from_mm, open_to_mm, daily_fee) VALUES (@archesid, 'Canyon Wren Group Site', 1, 12, 160.00);
declare @canyonid int = (SELECT @@IDENTITY)

-- Fill in site info
INSERT INTO site (site_number, campground_id, accessible, utilities) VALUES (1, @blackwoodsid, 1, 1);
declare @site1id int = (SELECT @@IDENTITY)
INSERT INTO site (site_number, campground_id, accessible, utilities) VALUES (2, @blackwoodsid, 1, 1);
declare @site2id int = (SELECT @@IDENTITY)
INSERT INTO site (site_number, campground_id, accessible, utilities) VALUES (3, @seawallid, 1, 1);
declare @site3id int = (SELECT @@IDENTITY)
INSERT INTO site (site_number, campground_id, accessible, utilities) VALUES (4, @seawallid, 1, 1);
declare @site4id int = (SELECT @@IDENTITY)
INSERT INTO site (site_number, campground_id, accessible, utilities) VALUES (5, @devilsid, 1, 1);
declare @site5id int = (SELECT @@IDENTITY)
INSERT INTO site (site_number, campground_id, accessible, utilities) VALUES (6, @devilsid, 1, 1);
declare @site6id int = (SELECT @@IDENTITY)
INSERT INTO site (site_number, campground_id, accessible, utilities) VALUES (7, @canyonid, 1, 1);
declare @site7id int = (SELECT @@IDENTITY)
INSERT INTO site (site_number, campground_id, accessible, utilities) VALUES (8, @canyonid, 1, 1);
declare @site8id int = (SELECT @@IDENTITY)

-- Fill in reservation info
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site1id, 'Smith Family Reservation', GETDATE()-2, GETDATE()+2);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site1id, 'Lockhart Family Reservation', GETDATE()-6, GETDATE()-3);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site2id, 'Jones Reservation', GETDATE()-2, GETDATE()+2);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site2id, 'Bauer Family Reservation', GETDATE(), GETDATE()+2);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site3id, 'Eagles Family Reservation', GETDATE()+5, GETDATE()+10);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site3id, 'Aersomith Reservation', GETDATE()-3, GETDATE()+2);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site4id, 'Claus Family Reservation', GETDATE(), GETDATE()+1);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site4id, 'Gunzenhauser Family Reservation', GETDATE()-5, GETDATE()-3);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site5id, 'Bentivegna Family Reservation', GETDATE()-6, GETDATE()-3);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site5id, 'Morel Reservation', GETDATE()-2, GETDATE()+2);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site6id, 'Galinas Family Reservation', GETDATE(), GETDATE()+2);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site6id, 'Hoggett Family Reservation', GETDATE()+5, GETDATE()+10);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site7id, 'Wilthew Reservation', GETDATE()-3, GETDATE()+2);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site7id, 'Fella Family Reservation', GETDATE(), GETDATE()+1);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site8id, 'Mordarski Reservation', GETDATE()-3, GETDATE()+2);
INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (@site8id, 'Holmes Family Reservation', GETDATE(), GETDATE()+1);

select @acadiaid as acadiaid, @blackwoodsid as blackwoodsid, @site1id as site1id